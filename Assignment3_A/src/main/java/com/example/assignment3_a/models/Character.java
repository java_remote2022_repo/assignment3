package com.example.assignment3_a.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50,nullable = false)
    private String name;
    @Column(length = 50,nullable = false)
    private String alias;
    @Column
    private Enums.Gender gender;
    @Column
    private String picture; //URL to a picture of the character
    @ManyToMany
}
