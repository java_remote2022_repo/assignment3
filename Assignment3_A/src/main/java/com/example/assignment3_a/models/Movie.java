package com.example.assignment3_a.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.cloud.CloudPlatform;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100,nullable = false)
    private String title;
    @Column(length = 100,nullable = false)
    private String genre;
    @Column
    private int releaseYear;
    @Column(length = 50,nullable = false)
    private String director;
    @Column
    private String picture; //URL to a movie poster
    @Column
    private String trailer; //URL to a youtube trailer link
}
